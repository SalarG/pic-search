import axios from 'axios';


export default axios.create({
    baseURL: 'https://api.unsplash.com',
    headers: {
        Authorization: 'Client-ID e45dd8214482959edd154aa701d625ec41ef620735c6a578fcefd6532b76f18d'
    }

});